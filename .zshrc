bindkey -e

HISTFILE=$HOME/.zhistory
HISTSIZE=100000
SAVEHIST=100000
REPORTTIME=3
autoload -U compinit
compinit -u

bindkey "[1~" beginning-of-line
bindkey "[4~" end-of-line
bindkey "[3~" delete-char
bindkey "[5~" up-line-or-history
bindkey "[6~" down-line-or-history

### options
setopt hist_ignore_dups
setopt share_history
setopt auto_pushd
setopt auto_cd
setopt list_packed
setopt no_flow_control
setopt no_beep
setopt ignore_eof
setopt long_list_jobs
setopt magic_equal_subst
setopt noclobber

### completion
# see https://raw.github.com/git/git/master/contrib/completion/git-completion.zsh
zstyle ':completion:*:*:git:*' script ~/.git-completion.sh
fpath=(~/.zsh/completion $fpath)

### utility functions
function mvln() {
    function __mvln() {
        local opts=$1
        local src=$2
        local dst=$3
        if [ -d $dst ]; then
            # When dst is dir, dst_file must be dir/filename
            local file=${src##*/}
            mv $opts -- $src $dst && ln -s -- ${dst}/${file} $src
        else
            mv $opts -- $src $dst && ln -s -- $dst $src
        fi
    }

    ### get options for mv
    local OPTIND o opts
    while getopts "bfv" o; do
        case "${o}" in
            [bfv])
                opts="${opts}${o}"
                ;;
            *)
                return
        esac
    done
    shift $((OPTIND-1))

    if [ "${opts}" != "" ]; then
        opts="-${opts}"
    fi

    ### mv + ln -s
    if [ $# -lt 2 ]; then
        echo "mvln: too few arguments."
        return
    elif [ $# -eq 2 ]; then
        __mvln "$opts" $1 $2
    else
        local dst_dir
        for dst_dir; do true; done  # get the last argument

        if [ ! -d $dst_dir ]; then
            mkdir -p $dst_dir || return
        fi

        for src_file in ${@:1:${#}-1}; do
            local dst_file=${dst_dir}/${src_file}
            __mvln "$opts" $src_file $dst_file
        done
    fi
}

function lth() {
    if [ -z $1 ]; then
        ls -lt
    else
        ls -lt | head $1
    fi
}

function lthh() {
    if [ -z $1 ]; then
        ls -lth
    else
        ls -lth | head $1
    fi
}

### prompt
## colors
local GREEN=$'%{\e[1;32m%}'
local YELLOW=$'%{\e[1;33m%}'
local BLUE=$'%{\e[1;34m%}'
local DEFAULT=$'%{\e[1;m%}'
## git
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
zstyle ':vcs_info:*' formats ' [%b]%u'
zstyle ':vcs_info:*' actionformats '[%b|%a]%u'
precmd() {
	psvar=()
	LANG=en_US.UTF-8 vcs_info
	psvar[1]=$vcs_info_msg_0_
}
## PROMPT
setopt PROMPT_SUBST
PROMPT="${GREEN}%n@%m %~${YELLOW}"$'\n'"%%${DEFAULT} "
RPROMPT="${YELLOW}%1v${DEFAULT}"

### alias
alias rm='rm -i'
alias ls='ls -ha --color=auto'
alias ll='ls -al'
alias lt='ls -alt'
alias screen='screen -U'
alias sc='screen'
alias callgrind='valgrind --tool=callgrind'
alias mkdir-sbt='mkdir -p src/main/scala; mkdir -p src/test/scala; mkdir project'
alias wget='wget --content-disposition'
alias tm=tmux
alias x=exit

### core
ulimit -S -c 1073741824 > /dev/null 2>&1

#/bin/sh
CTAGS_PATH=`which ctags`
if [ ! -x $CTAGS_PATH ] ; then
    echo "ctags: not found."
    exit
fi

BOOST_PATH=/usr/include/boost
if [ ! -d $BOOST_PATH ] ; then
    echo "boost header: not found."
    exit
fi

CPPUNIT_PATH=/usr/include/cppunit
if [ ! -d $CPPUNIT_PATH ] ; then
    echo "cppunit header: not found."
    exit
fi

BOOST_FILE_LIST=/tmp/boost.list
find $BOOST_PATH -regex ".+\(h\|hpp\)" | grep -v "./typeof/*" > $BOOST_FILE_LIST
echo "generating boost tags."
ctags --sort=foldcase --languages=c++ --fields=+aS --c++-kinds=+p -f ~/.vim/tags/boost -L $BOOST_FILE_LIST
echo "generating cppunit tags."
ctags -R --sort=foldcase --languages=c++ --fields=+iaS --c++-kinds=+p -f ~/.vim/tags/cppunit $CPPUNIT_PATH

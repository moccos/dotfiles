#!/bin/sh
if [ ! -e ~/.vim/bundle/vundle ] ; then
    mkdir -p ~/.vim/bundle
    git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
fi

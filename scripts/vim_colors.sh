#!/bin/sh
mkdir -p ~/.vim/colors
cd ~/.vim/colors
if [ ! -e molokai.vim ] ; then
    ln -s ~/.vim/bundle/molokai/colors/molokai.vim
fi
if [ ! -e mrkn256.vim ] ; then
    ln -s ~/.vim/bundle/mrkn256.vim/colors/mrkn256.vim
fi

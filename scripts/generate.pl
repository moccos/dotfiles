#!/usr/bin/perl
use strict;
use warnings;
use FindBin;

our $output_dir_name = '../out';
our $root_dir = '..';

# subroutines
sub simple_merge_files {
    my $dir_name = shift;
    my $file_name = shift;
    my $command = "cat $root_dir/$file_name $dir_name/$file_name > ./$output_dir_name/$file_name";
    print($command . "\n");
    system($command);
}

# main
our $script_path = $FindBin::Bin;
chdir $script_path;

mkdir($output_dir_name);
my $os = "";
if (defined($ENV{'OS'})) {
    $os = substr($ENV{'OS'}, 0, 3);
}

if ($os eq 'Win') {
    # CYGWIN
    my $sub_dir = "$root_dir/_cygwin";
    unless (-e $sub_dir) {
        print "target directory " . $sub_dir . " is not found.\n";
        exit();
    }
    simple_merge_files($sub_dir, ".zshrc");
    simple_merge_files($sub_dir, ".zshenv");
} else {
    print "nothing to do.\n";
}

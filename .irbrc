require 'wirble'
require 'irb/completion'
require 'irb/ext/save-history'
Wirble.init
Wirble.colorize
IRB.conf[:SAVE_HISTORY] = 10000
IRB.conf[:AUTO_INDENT] = true
IRB.conf[:USE_READLINE] = true

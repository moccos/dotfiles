export SHELL=/bin/zsh
export GOPATH=~/bin/go
export PATH=/usr/local/bin:/usr/bin:/sbin:$PATH:~/scripts:$GOPATH/bin
export JAVA_OPTS="-Dfile.encoding=UTF-8 -XX:+CMSClassUnloadingEnabled"
if [ -f $HOME/.cargo/env ]; then
    source $HOME/.cargo/env
fi

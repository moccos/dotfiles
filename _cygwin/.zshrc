### Windows only
alias exp='explorer .'
alias .=exp
alias wpwd='/bin/pwd | xargs cygpath -w'
wcd() { cd `cygpath $*`; }
alias apt-cyg='apt-cyg -m "ftp://ftp.jaist.ac.jp/pub/cygwin"'
alias cs=cygstart
alias vi=vim
alias java='java -Duser.language=en'
alias javac='javac -J-Duser.language=en'

function exekill() {
    if [ "$1" = "" ]; then
        echo "exekill: specify exe name (without .exe)"
        return
    fi
    local exe=$1.exe
    taskkill /f /im $exe
}

### Windows code page
chcp 65001 > /dev/null

if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2

iab <buffer> u8 uint8_t
iab <buffer> u16 uint16_t
iab <buffer> u32 uint32_t
iab <buffer> u64 uint64_t
iab <buffer> i8 int8_t
iab <buffer> i16 int16_t
iab <buffer> i32 int32_t
iab <buffer> i64 int64_t
iab <buffer> #i #include
iab <buffer> #d #define
iab <buffer> #e #endif

iab <buffer> rcast reinterpret_cast<><Left>
iab <buffer> ccast const_cast<><Left>
iab <buffer> scast static_cast<><Left>

iab <buffer> np nullptr
iab <buffer> ce constexpr
iab <buffer> noex noexcept
iab <buffer> exp explicit
iab <buffer> vir virtual

iab <buffer> main_ int main(int argc, char **argv) {}<Left>

syn keyword nullptr nullptr
hi link nullptr Constant

syn keyword cpp11type noexcept
syn keyword cpp11type constexpr
syn keyword cpp11type decltype
hi link cpp11type Type

syn keyword malloctype malloc
syn keyword malloctype free
hi link malloctype Type

if has('path_extra')
    set tags+=~/.vim/tags/boost;
endif

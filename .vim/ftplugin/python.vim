if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

setlocal smartindent

IndentGuidesEnable
" let g:indent_guides_enable_on_vim_startup=1
let g:indent_guides_start_level = 2
let g:indent_guides_auto_colors = 0
let g:indent_guides_guide_size = 1
hi IndentGuidesOdd ctermbg=green
hi IndentGuidesEven ctermbg=cyan

let g:pydiction_location = '~/.vim/bundle/pydiction/complete-dict'

if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

iab <buffer> u8 uint8_t
iab <buffer> u16 uint16_t
iab <buffer> u32 uint32_t
iab <buffer> u64 uint64_t
iab <buffer> i8 int8_t
iab <buffer> i16 int16_t
iab <buffer> i32 int32_t
iab <buffer> i64 int64_t
iab <buffer> #i #include
iab <buffer> #d #define
iab <buffer> #e #endif
iab <buffer> #u #undef

iab <buffer> main_ int main(int argc, char **argv) {}<Left>
iab <buffer> np NULL

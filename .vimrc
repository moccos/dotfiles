" *** Vim Options ***
" Vim全体の挙動
set nocompatible
set nomodeline
" ファイル関係
set autoread
set hidden
set nobackup
set noswapfile
" set nofixeol  " 7.4.785以降

augroup vimrc-checktime
  autocmd!
  autocmd WinEnter * checktime
augroup END

" 入力
set backspace=indent,eol,start
set formatoptions=lmoq

" 画面表示
set background=dark
set laststatus=2
set number
set scrolloff=2
set showcmd
set showmode
set showmatch
set textwidth=0

" Format
set autoindent
set shiftwidth=4
set expandtab
set smarttab
set tabstop=4
set softtabstop=0

" Search
set hlsearch
set ignorecase
set smartcase

" その他
syntax on
colorscheme mrkn256

if has('path_extra')
    set tags+=tags;
    " set tags+=./**5/tags;
endif

" *** プラグイン関係の設定 ***
" matchit
source $VIMRUNTIME/macros/matchit.vim

" Syntastaicの対象
let g:syntastic_mode_map = {
            \'mode': 'passive',
            \'active_filetypes': [
                \'ruby', 'javascript', 'html', 'xml', 'yaml', 'haxe', 'ocaml'
            \],
            \'passive_filetypes': [] }

" *** 挿入モード ***
" 挿入モード強調
autocmd InsertEnter,InsertLeave * set cursorline!
" 邪道
inoremap <C-a> <Home>
inoremap <C-e> <End>
inoremap <C-o> <CR>
inoremap <C-d> <Delete>
" 頻繁に誤爆するバッファ操作用C-wを強引に通す
inoremap <C-w> <ESC><C-w>
" 移動系はC-hでBackspaceが出ないので
" inoremap <C-h> <Left>
" カーソルキー (いらんかも)
inoremap OA <Up>
inoremap OB <Down>
inoremap OC <Right>
inoremap OD <Left>

" *** コマンドライン ***
cnoremap <C-a> <Home>
cnoremap <C-e> <End>
cnoremap <C-o> <CR>
cnoremap <C-d> <Delete>

" *** 通常モード・ビジュアルモード ***
" 上下移動
nnoremap j gj
nnoremap k gk
nnoremap OB gj
nnoremap OA gk
" カーソルキー誤爆防止
nnoremap OA <Up>
nnoremap OB <Down>
nnoremap OD <Left>
nnoremap OC <Right>

" 色々トグル
noremap <Leader>tw :<C-u>setlocal wrap!<CR>
noremap <silent> <Leader>tn :<C-u>setlocal number!<CR>
noremap <silent> <Leader>tp :<C-u>setlocal paste!<CR>
noremap <silent> <Leader>tl :<C-u>setlocal list!<CR>
" 改行動作 (行末の挙動のせいで意外と使えない)
nnoremap <CR> i<CR><ESC>$
" 検索ハイライト解除
nnoremap <silent> <ESC><ESC> :<C-u>nohlsearch<CR>
" gitの都合でよく使う
nnoremap <Leader>ct :<C-u>checktime<CR>

" smartword
map ,w  <Plug>(smartword-w)
map ,b  <Plug>(smartword-b)
map ,e  <Plug>(smartword-e)
map ,ge  <Plug>(smartword-ge)

" operator-camelize
map <Leader>C <Plug>(operator-camelize)
map <Leader>c <Plug>(operator-decamelize)

" operator-replace
map R <Plug>(operator-replace)

" textmanip
vmap <C-j> <Plug>(textmanip-move-down)
vmap <C-k> <Plug>(textmanip-move-up)
vmap <C-l> <Plug>(textmanip-move-right)
vmap <C-h> <Plug>(textmanip-move-left)
" textmanip-duplicateよりはypの方が便利のような

" onlyからの分割
nnoremap <Leader>av :<C-u>only<CR>:AV<CR>
nnoremap <Leader>as :<C-u>only<CR>:AS<CR>

" *** 未分類 ***
filetype plugin on
source ~/.vim/.vimrc.bundle

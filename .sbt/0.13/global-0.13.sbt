import AssemblyKeys._

// official sample
shellPrompt := { state =>
 "sbt (%s)> ".format(Project.extract(state).currentProject.id)
}

scalariformSettings

assemblySettings

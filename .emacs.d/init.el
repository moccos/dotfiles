;;; basic settings
(setq default-tab-width 4)
(menu-bar-mode -1)
(show-paren-mode 1)
(column-number-mode t)
(line-number-mode t)
(auto-save-mode nil)
(setq backup-inhibited t)
(setq delete-auto-save-files t)
(setq make-backup-files nil)
(setq auto-save-default nil)

;;; keys
(global-set-key [delete] 'delete-char)
(global-set-key [select] 'end-of-line)
(define-key global-map (kbd "M-o h") 'beginning-of-buffer)
(define-key global-map (kbd "M-o f") 'end-of-buffer)
(define-key global-map (kbd "C-c y") 'yank)
(define-key global-map (kbd "C-f") 'other-window)
(define-key global-map (kbd "C-j") 'goto-line)
(define-key global-map (kbd "C-h") 'delete-backward-char) 
(define-key global-map (kbd "M-?") 'help-for-help)        
(define-key global-map (kbd "C-c i") 'indent-region)      
(define-key global-map (kbd "C-c C-i") 'hippie-expand)    
(define-key global-map (kbd "C-c ;") 'comment-dwim)       

;;; disable vc
(eval-after-load "vc" '(remove-hook 'find-file-hooks 'vc-find-file-hook))
(remove-hook 'find-file-hooks 'vc-find-file-hook)
(delete 'Git vc-handled-backends)
(setq vc-handled-backends ())

;;; Erlang
;(setq load-path (cons  "/cygdrive/e/bin/erl/lib/tools-2.6.6.6/emacs"
;                       load-path))
;(setq erlang-root-dir "/cygdrive/e/bin/erl")
;(setq exec-path (cons "/cygdrive/e/bin/erl/bin" exec-path))
;(require 'erlang-start)

;;; Scala mode
; http://d.hatena.ne.jp/tototoshi/20100925/1285420294
; (add-to-list 'load-path "~/.emacs.d/scala-mode")
; (require 'scala-mode)
;; (add-to-list 'auto-mode-alist '("\\.scala$" . scala-mode))

;(add-hook 'scala-mode-hook
;		  (function
;		   (lambda ()
;			 (scala-mode-lib:define-keys scala-mode-map
;										 ([(shift tab)]   'scala-undent-line)
;										 ([(control tab)] nil))
;			 (local-set-key [(return)] 'newline-and-indent))))
;(add-hook 'scala-mode-hook 'jaspace-mode)

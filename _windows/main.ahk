; +Shift !Alt ^Ctrl

#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
SetTitleMatchMode RegEx

GroupAdd GroupIrf, ahk_class IrfanView 
GroupAdd GroupIrf, ahk_class FullScreenClass 

; Kill Ins
Ins::Send {}

#IfWinActive ahk_class mintty
{
  !v::Send +{Ins}
  !c::Send ^{Ins}
}

; madokae
#IfWinActive ahk_class TForm1
{
  !w::Send {}
}

#IfWinActive ahk_class OperaWindowClass
{
  +^w::Send {}
  ^k::Send {F8}  ; Search
  ^Tab::Send ^{F6} ; Next tab
  ^+Tab::Send ^+{F6} ; Prev tab
}

; Chrome
#IfWinActive ahk_class Chrome_WidgetWin_1
{
  +^w::Send {} ; Disable "Close current window"
  +^q::Send {} ; Disable "Close all windows"
}

#IfWinActive ahk_class PPTFrameClass
{
  ^w::Send {}
  ^q::Send {}
}

; Excel
#IfWinActive ahk_class XLMAIN
{
  ; Move to next/prev tab
  ^Tab::Send ^{PgDn}
  ^+Tab::Send ^{PgUp}
}

#IfWinActive ^OpenTween
{
  ^s::Send {}
  ^+t::Send {}
}

#IfWinActive GroupIrf
{
  F9::Send {SPACE}
  F11::Send {BS}
}

; Irfanview dialog, Vocaloid3 Editor Dialog
#IfWinActive ahk_class #32770
{
  MButton::Send {ESC}
  +RButton::Send {ESC}
  F12::Send {ESC}
}

#IfWinActive, ^VOCALOID Editor.+
{
  F9 & WheelUp::Send +{Tab}
  F9 & WheelDown::Send {Tab}
  F9 & LButton::Send ^!{PgUp}
  F9 & RButton::Send ^!{PgDn}
  F11 & WheelUp::Send ^{PgUp}
  F11 & WheelDown::Send ^{PgDn}
  F11 & MButton::Send {F2}
  F11 & LButton::Send !{Up}
  F11 & RButton::Send !{Down}
  F11 up::Send {Esc}
  F12 & WheelUp::Send {NumpadSub}
  F12 & WheelDown::Send {NumpadAdd}
  F12 & LButton::Send ^w
  F12 & RButton::Send ^e
  +F9::Send {NumpadSub}
  +F11::Send {NumpadAdd}
  +F12::Send {Enter}
}

#IfWinActive, ^VOCALOID3 Editor.+
{
  ; #Win !Alt ^Ctrl +Shift
  ; Play / Stop
  F11::Send {Enter}
  MButton::Send {Enter}

  ; move to next/prev bar
  F12::Send !rf
  +F12::Send !rr
  +WheelUp::Send !rr
  +WheelDown::Send !rf

  ; move to next/prev note {Tab}

  ; zoom
  ^WheelUp::Send ^{Right}
  ^WheelDown::Send ^{Left}

  ; redo
  ^+z::Send !er

  ; export wave
  ^+F9::Send !few
  ^+MButton::Send !few

  ; other
  +F9::Send !jn
  ^F9::Send !jj

}

#IfWinActive, .+ - VLC.+$
{
  F9::Send {SPACE}
  F11::Send !3
  F12::Send !4
}

; Explorer
#IfWinActive ahk_class CabinetWClass
{
  F9::F3
  F12::
    Run explorer.exe Z:
  F1::return
  ^z::return
}

#IfWinActive, .+SONAR 8.5 Producer$
{
  +F9::Send ^+{PgUp}
  +F11::Send ^+{PgDn}
}

#IfWinActive, .+Domino$
{
  +F9::Send +s
  +F11::Send +w
  ^F12::Send ^b
  F9::Send ^{Left}
  F11::Send ^{Right}
}

#IfWinActive, .+Thunderbird$
{
  F9::Send +^t
  +F11::Send n
  F11::Send {Enter}
  F12::Send {vkF3sc029}
}

#IfWinActive, .+Firefox$
{
  F1::return
  +F11::
  F9::
    Send +^{Tab}
    return
  F11::Send ^{Tab}
  F12::Send {vkF3sc029}
}

#IfWinActive, .*main.ahk.*
{
  F12::
    Send ^s
    Reload
    return
  F9::Send {#}IfWinActive{,}{Enter}{{}{Enter}{Space 2}{Enter}{}}{Enter}{Up 3}{Left}
}
